const path = require('path')
module.exports = {
  publicPath: "/",
  lintOnSave: true, // 是否使用eslint
  outputDir: 'dist',
  productionSourceMap: false,

  devServer: {
    proxy: {
      '/api': {
        target: 'http://172.27.30.203:8080',
        changeOrigin: true, // 开启代理
        ws: true, // 是否启用webSockets
        pathRewrite: {
          '^/api': ''
        }
      },
      '/vitriol': {
        target: "http://10.10.4.106:32655",
        ws: true,
        changeOrigin: true,
        pathRewrite: { '^/vitriol': '/vitriol' }
      },
      '/anode': {
        target: "http://10.10.4.110:32655",
        ws: true,
        changeOrigin: true,
        pathRewrite: { '^/anode': '/anode' }
      },
    },
    open: false,//配置自动启动浏览器
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // 'blue': '#6fe2be',
          hack: `true; @import "${path.join(
            __dirname,
            "./src/assets/css/custom-vant.less"
          )}";`
        },
      },
    },
  },
}