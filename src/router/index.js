import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login/index.vue";
import Test from './test'
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  Test,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title === undefined ? 'vue_vant' : to.meta.title
  next()
})


export default router;
