export default {
  path: '/test',
  name: 'Test',
  redirect: '/home',
  component: () => import('../views/Test'),
  children: [
    {
      path: '/home',
      name: 'Home',
      component: () => import('../views/Test/home.vue'),
      meta: {
        title: 'home'
      }
    },
    {
      path: '/content',
      name: 'Content',
      component: () => import('../views/Test/content.vue'),
      meta: {
        title: 'content'
      }
    },
    {
      path: '/person',
      name: 'Person',
      component: () => import('../views/Test/person.vue'),
      meta: {
        title: 'person'
      }
    },
  ]
}