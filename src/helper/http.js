import axios from 'axios'

// 创建axios实例
const request = axios.create({
  baseUrl: '',
  timeout: 9 * 1000
})

request.interceptors.request.use((config) => {
  // if (helper.storage.get('token')) {
  //   Object.assign(config.headers, { authorization: helper.storage.get('token'), 'Osso-User-Guid': '04471' })
  // }
  return config
}, (error) => {
  return Promise.reject(error)
})

request.interceptors.response.use((response) => {
  return response.data
}, (error) => {
  console.log(error.message)
  // helper.msg.error(error.response.data.msg || error.message)
  // if (error.response.status === 404) {
  //   helper.msg.error(error.message)
  // }
  // if (error.response.status === 500 && error.response.data.message === 'Token失效，请重新登录') {
  //   helper.msg.error('登录超时')
  //   helper.storage.clear()
  //   router.push({ name: 'login' })
  // } else {
  //   helper.msg.error(error.message)
  // }
})

const http = (url, data, options = {}) => {
  const result = request({
    url: url,
    method: options.method,
    params: options.method === 'get' ? data : {},
    data: data
  })
  return result
}
const get = (url, data, options = {}) => {
  options.method = 'get'
  return http(url, data, options)
}

const post = (url, data, options = {}) => {
  options.method = 'post'
  return http(url, data, options)
}

const put = (url, data, options = {}) => {
  options.method = 'put'
  return http(url, data, options)
}

const del = (url, data, options = {}) => {
  options.method = 'delete'
  return http(url, data, options)
}

export default {
  http,
  get,
  post,
  put,
  del,
}