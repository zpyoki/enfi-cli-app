import Vue from 'vue';

import { Button, Tabbar, TabbarItem, Notify, Toast } from 'vant';

Vue.use(Button);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Notify);
Vue.use(Toast);